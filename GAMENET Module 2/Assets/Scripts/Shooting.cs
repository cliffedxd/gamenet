﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("Health Related Stuff")]
        public float startHealth = 100;
    private bool isDead;
    private float health;
    public Image healthBar;
    private Animator animator;

    [Header("Kill Info")]
    public int killCount;
    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
        health = startHealth;
        healthBar.fillAmount = health/startHealth;
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(killCount >= 10)
        {
            StartCoroutine(ShowWinner());
        }
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);

                if(hit.collider.gameObject.GetComponent<Shooting>().health <= 0 && !hit.collider.gameObject.GetComponent<Shooting>().isDead)
                {
                    hit.collider.gameObject.GetComponent<Shooting>().isDead = true;
                    photonView.RPC("AddKill", RpcTarget.AllBuffered);
                }
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health/startHealth;

        if(health <= 0)
        {
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            StartCoroutine(KillText());

            IEnumerator KillText()
            {
                GameObject killText = GameObject.Find("Kill Text");
                killText.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
                yield return new WaitForSeconds(2f);
                killText.GetComponent<Text>().text = "";
            }
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        photonView.RPC("ResetIsDead", RpcTarget.AllBuffered);
        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        Transform randomPoint = SpawnManager.instance.ChooseRandomSpawnPoint();
        
        this.transform.position = randomPoint.transform.position;
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("ResetHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void ResetHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void AddKill()
    {
        killCount++;
        Debug.Log(photonView.Owner.NickName + " Kills: " + this.killCount);
    }

    [PunRPC]
    public void ResetIsDead()
    {
        isDead = false;
    }

    IEnumerator ShowWinner()
    {
        GameObject winText = GameObject.Find("Win Text");
        float winTime = 5.0f;

        while(winTime > 0)
        {
            yield return new WaitForSeconds(1f);
            winTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            winText.GetComponent<Text>().text = photonView.Owner.NickName + " won the game. Returning to lobby in "
                + winTime.ToString();
        }

        LeaveRoom();
    }

    public void LeaveRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }
}
