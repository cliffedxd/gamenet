﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] spawnPointList;

    public static SpawnManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }

        else
        {
            instance = this;
        }
    }
    
    public Transform ChooseRandomSpawnPoint()
    {
        int random = Random.Range(0, spawnPointList.Length);

        return spawnPointList[random].transform;
    }
}
