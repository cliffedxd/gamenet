﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector2 movement;
    new Rigidbody2D rigidbody2D;
    public float moveSpeed;
    public bool isControlEnabled;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        isControlEnabled = false;

        if(NormalGameModeController.instance)
            moveSpeed = 5f;
        else if(HyperGameModeController.instance)
            moveSpeed = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        if(isControlEnabled)
        {
            movement.x = Input.GetAxis("Horizontal");
            movement.y = Input.GetAxis("Vertical");
        }
    }

    private void FixedUpdate()
    {
        if(isControlEnabled)
            rigidbody2D.MovePosition(rigidbody2D.position + movement * moveSpeed * Time.fixedDeltaTime);
    }


}
