﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI playerNameText;
    // Start is called before the first frame update
    void Start()
    {
        playerNameText.text = photonView.Owner.NickName;

        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("normal"))
        {
            GetComponent<PlayerController>().enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("hyper"))
        {
            GetComponent<PlayerController>().enabled = photonView.IsMine;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
