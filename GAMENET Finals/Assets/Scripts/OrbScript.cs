﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class OrbScript : MonoBehaviourPunCallbacks
{
    public int orbCount;

    public enum RaiseEventsCode
    {
        OrbsCollected50EventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.OrbsCollected50EventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;
            string nameOfPlayer = (string)data[0];
            Debug.Log(nameOfPlayer + " has collected 50 orbs.");
            GameObject orbCollectionText = GameObject.Find("OrbText");
            orbCollectionText.GetComponent<Text>().text = nameOfPlayer + " has collected 50 orbs.";
            
        }
    }
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if(NormalGameModeController.instance)
        {
            if(orbCount >= 50)
                CallEvent();
            if(NormalGameModeController.instance.isGameFinished)
                if(orbCount >= 50)
                    StartCoroutine(ShowWinner());
        }

        if(HyperGameModeController.instance)
        {
            if(orbCount >= 50)
                CallEvent(); 
            if(HyperGameModeController.instance.isGameFinished)
                if(orbCount >= 100)
                    StartCoroutine(ShowWinner());
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Orb"))
        {
            photonView.RPC("AddOrb", RpcTarget.AllBuffered);
            Destroy(other.gameObject);
        }    
    }

    [PunRPC]
    public void AddOrb()
    {
        orbCount++;
    }

    public void CallEvent()
    {
        string nickName = photonView.Owner.NickName;
        object[] data = new object[] {nickName};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.OrbsCollected50EventCode, data, raiseEventOptions, sendOptions);
    }

    IEnumerator ShowWinner()
    {
        GameObject winText = GameObject.Find("winText");
        float winTime = 3.0f;

        while(winTime > 0)
        {
            yield return new WaitForSeconds(1f);
            winTime--;

            winText.GetComponent<Text>().text = photonView.Owner.NickName + " has won the game.";
        }
        winText.GetComponent<Text>().text = "";
        GetComponent<PlayerController>().enabled = false;
        Time.timeScale = 0;
    }
}
