﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public Text timerText;
    public float timeToStartGame = 5f;
    // Start is called before the first frame update
    void Start()
    {
        if(NormalGameModeController.instance)
            timerText = NormalGameModeController.instance.timerText;
        if (HyperGameModeController.instance)
            timerText = HyperGameModeController.instance.timerText;
    }

    // Update is called once per frame
    void Update()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            if(timeToStartGame > 0)
            {
                timeToStartGame -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartGame);
            }
            else if(timeToStartGame < 0)
            {
                photonView.RPC("StartGame", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void SetTime(float time)
    {
        if (time > 0)
            timerText.text = time.ToString("F1");
        else
            timerText.text = "";
    }

    [PunRPC]
    public void StartGame()
    {
        if(NormalGameModeController.instance)
            StartCoroutine(NormalGameModeController.instance.PlayGame());
        if (HyperGameModeController.instance)
            StartCoroutine(HyperGameModeController.instance.PlayGame());

        GetComponent<PlayerController>().isControlEnabled = true;
        this.enabled = false;
    }
}
