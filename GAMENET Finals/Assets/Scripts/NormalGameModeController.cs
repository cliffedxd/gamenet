﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class NormalGameModeController : MonoBehaviour
{
    public GameObject spawnArea;
    BoxCollider2D spawnAreaCollider;
    public GameObject orbPrefab;
    public GameObject[] characterPrefabs;
    public Transform[] startingPositions;
    public Text timerText;
    public bool isGameFinished = false;

    public static NormalGameModeController instance = null;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        spawnAreaCollider = spawnArea.GetComponent<BoxCollider2D>();
        //StartCoroutine(PlayGame());

        if(PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log("Player Selection Number: " + (int) playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 insantiatePosition = startingPositions[actorNumber-1].position;
                PhotonNetwork.Instantiate(characterPrefabs[(int) playerSelectionNumber].name, insantiatePosition, Quaternion.identity);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator PlayGame()
    {
        float gameTime = 61f;
        Bounds spawnBounds = spawnAreaCollider.bounds;
        GameObject timerText = GameObject.Find("Timer");

        while(gameTime > 0)
        {
            yield return new WaitForSeconds(1f);
            gameTime--;

            timerText.GetComponent<Text>().text = "" + gameTime.ToString();

            float posX = Random.Range(spawnBounds.min.x, spawnBounds.max.x);
            float posY = Random.Range(spawnBounds.min.y, spawnBounds.max.y);
            Vector2 pos = new Vector3(posX, posY, 0);
            
            //GameObject orbObject = Instantiate(orbPrefab, pos, Quaternion.identity);
            GameObject orbPhoton = PhotonNetwork.Instantiate("orb", pos, Quaternion.identity);
        }

        isGameFinished = true;
    }
}
